#include "DIALOG.h"
#include "arm_math.h"

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DSP_H
#define __DSP_H
#include <stdint.h>

#define M_PI	(double)(3.14159265359)
#define F_UPDATE	10000.0
#define F_SIGNAL	50.0
#define NPP			(int)(F_UPDATE/F_SIGNAL)



#define GRAPH_SIZE_X 400
#define GRAPH_SIZE_Y 280
#define GRAPH_SIZE_BORDER_X 5
#define GRAPH_SIZE_BORDER_Y 5

#define DMA_QUEQUE_SIZE		GRAPH_SIZE_X

#define FFT_VISUAL_OFFSET 12 //dovuto ai bordi
#define FFT_NP (400 - (FFT_VISUAL_OFFSET)) 


typedef enum {
	_500_us,	//TODO: verificare se ce la facciamo ad arrivare a 500 us a divisione con la dimensione del buffer scelta
	_1_ms,		
	_2_ms,
	_5_ms,
	_10_ms,
	_20_ms,
	_50_ms,
	_100_ms,
	_200_ms,
	_500_ms,
	_1_s,
	MAX_HSCALE
}h_scale_t;


typedef enum {
	_1_mV,
	_2_mV,
	_5_mV,
	_10_mV,
	_20_mV,
	_50_mV,
	_100_mV,
	_200_mV,
	_500_mV,
	_1_V,
	_2_V,
	_5_V,
	MAX_VSCALE
}v_scale_t;

typedef enum
{
  up,
  down,
} edge_t;

typedef enum
{
  CH_1,
  CH_2,
} channel_t;

typedef enum
{
	pre,
	half,
	post,
} trigger_conf;

extern WM_HWIN hGraph;
extern GRAPH_DATA_Handle hData0;
extern GRAPH_DATA_Handle hData1;
extern GRAPH_SCALE_Handle hScale_v;
extern GRAPH_SCALE_Handle hScale_h;

extern int livello_trigger;
extern float fullscale_v;


extern float result_rms0;
extern float result_rms1;
extern float result_mean0;
extern float result_mean1;

extern void horizontal_scale(h_scale_t time_div);
extern void vertical_scale(v_scale_t V_div);
extern void apply_vertical_scale(uint16_t *samples, int size);
extern int trigger (uint16_t * input0, int size, int level, edge_t fronte, int isteresi);

extern float calculate_mean(uint16_t *samples, int size);
extern float calculate_rms(uint16_t *samples, int size);

extern void converti_in_fractional_normalizzati(uint16_t *integer_data, q15_t *fractional_data, int size);
extern void converti_da_fractional_normalizzati(q15_t *fractional_data, uint16_t *integer_data, int size);
extern void calcolo_fft(q15_t *samples, int samples_size);
extern void create_hanning_window(int L);


#endif	//__DSP_H

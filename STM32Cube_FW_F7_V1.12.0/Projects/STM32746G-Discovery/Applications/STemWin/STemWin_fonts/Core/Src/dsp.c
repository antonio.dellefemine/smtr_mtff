#include <math.h>
#include <stdbool.h>
#include "dsp.h"
#include "stm32f7xx_hal.h"
#include "DIALOG.h"

extern TIM_HandleTypeDef htim4;
extern bool fft_enable;

float result_rms0;
float result_rms1;
float result_mean0;
float result_mean1;

const int ta_10us[MAX_HSCALE] = {
	50, 
	100, 
	200, 
	500, 
	1000, 
	2000, 
	5000, 
	10000, 
	20000, 
	50000, 
	100000,
};

const int V_mV[MAX_VSCALE] = {
	1, 
	2, 
	5, 
	10, 
	20, 
	50,  
	100, 
	200, 
	500, 
	1000,
};

uint16_t sinusoidal_signal[NPP];
float fullscale_v = 1.0;
float fullscale_s = 1.0;

int livello_trigger = 1250;

int trigger(uint16_t * input0, int size, int level, edge_t fronte, int isteresi) 
{
	int32_t prod[size];
  int16_t input1[size];
  int i = 0;
	
	if (fronte == up)
	{
		level += isteresi;
	}
	else
	{
		level -= isteresi;
	}
	
 	// dal momento che avete deciso di fare il prodotto, secondo voi qual'e' il modo piu' indicato per scorrere un array
 	while (i < size - 1)
	{
		input1[i] = input0[i] - level;
    i++;
	}
  i = 1;
	while (i < size - 1) 
	{
	    prod[i] = ((int) input1[i]) * ((int) input1[i - 1]);
	    i++;
	}

  i = 0;
	if (fronte == up)
	{
		//continua finche' non trovo un prodotto negativo (attraversamento)
		//o finche' il campione e' negativo, perch� se negativo ho trovato un attraversamento con fronte di salita
		while ((prod[i] >= 0) || (input1[i] <= 0)) //applicato De Morgan
		{	
			i++;
			if (i >= size - 1)
			{
					return 0;
			}
		} 
	}
	else
	{
		//continua finche' non trovo un prodotto negativo (attraversamento)
		//o finche' il campione e' positivo, perch� se negativo ho trovato un attraversamento con fronte di salita
		while ((prod[i] >= 0) || (input1[i] >= 0)) 
		{	
			i++;
			if (i >= size - 1)
			{
					return 0;
			}
		} 
	}
	return i;
}

/*
 * horizontal scale 
 * @param t_div	division duration
 * return value to program adc trigger timer
 */
void horizontal_scale(h_scale_t time_div) 
{
  
  if (fft_enable) 
  {
    fullscale_s = (FFT_NP * FFT_NP * 10000L) / (1024.0 * ta_10us[time_div]);
  }
	else
	{
		fullscale_s = (100.0 * ta_10us[time_div]);
	}

	GRAPH_SCALE_SetFactor(hScale_h, fullscale_s/(GRAPH_SIZE_X - 10));
	
	if (time_div == _1_s)
	{ 
			__HAL_TIM_SET_PRESCALER(&htim4, 50);
			__HAL_TIM_SetAutoreload(&htim4, (ta_10us[time_div] / 2) - 1);
	}
	else
	{
		__HAL_TIM_SetAutoreload(&htim4, ta_10us[time_div] - 1);
		__HAL_TIM_SET_PRESCALER(&htim4, 25);
	}
	
}
	
void vertical_scale(v_scale_t V_div) 
{
  fullscale_v = (8.0 * V_mV[V_div]);
	
	GRAPH_SCALE_SetFactor(hScale_v, fullscale_v / (GRAPH_SIZE_Y - 16));
  fullscale_v /= 10;	
	// ci conserviamo fullscale decimi per evitare overflow nei calcoli successivi
}
	
	
//GENERAZIONE SEGNALE SINUSOIDALE
void prepare_signal(void) 
{
	int k = 0;
	while(1) {
		
		double val = 0.0;
		static double temp;
		  
		temp = 2 * M_PI * F_SIGNAL * k / F_UPDATE;
		val = sin(temp);
		  
		val *= 2047-408;
		val += 2048;
		
		sinusoidal_signal[k] = val;
		
		k++;
		if (k >= NPP) {
			break;
		}
	}
}
	
void apply_vertical_scale(uint16_t *samples, int size) 
{
  //TODO: OTTIMIZZAre usando la libreria dsp
  for (int i = 0; i < size; i++) 
  {
    float temp = 4095 * fullscale_v;
    samples[i] = (330L * GRAPH_SIZE_Y * samples[i]) / (temp);
		//moltiplichiamo per 330 per evitare l'overflow e per tenere conto del fattore 10 
  }
}
	
float calculate_mean(uint16_t *samples, int size) 
{
	volatile int64_t sum = 0;

	for(int i = 0; i < size; i++){
		sum += samples[i];
	}
	
	return ((3.3 * sum) / (size * 4095.0));
}

float calculate_rms(uint16_t *samples, int size) 
{
	volatile int64_t sum = 0;
	
	for(int i = 0; i < size; i++){
		sum += (samples[i]*samples[i]);
	}
	return (3.3 / 4095.0) * sqrt ((sum) / (size)); 
}

void converti_in_fractional_normalizzati(uint16_t *integer_data, q15_t *fractional_data, int size) 
{
	arm_shift_q15((q15_t *)integer_data, 3, fractional_data, size);
	//A QUESTO PUNTO IL DATO � NORMALIZZATO
	//Dobbiamo ridcordarci che abbiamo moltiplicato per un fattore 8
	//Dobbiamo anche ricordarci che abbiamo diviso per 32768 (cast)
}

void converti_da_fractional_normalizzati(q15_t *fractional_data, uint16_t *integer_data, int size) 
{
	arm_shift_q15(fractional_data, -1, (q15_t *) integer_data, size);
}


q15_t hanning_window_q15[1024];

void create_hanning_window(int L) 
{
	int i;
	// Create the Hanning window. This is usually done once at the
	// start of the program.
	for(i = 0; i < L; i ++) {
		
		hanning_window_q15[i] = (q15_t) (0.5f * 32467.0f * (1.0f - cosf(2.0f * PI * i / L)));
	}
}

void calcolo_fft(q15_t *samples, int samples_size)
{
	q15_t buffer_fft0[samples_size * 2];	//VLA
	arm_rfft_instance_q15 S;


// Apply the window to the input buffer
	arm_mult_q15(hanning_window_q15, samples, samples, samples_size);
	
	arm_rfft_init_q15(&S, samples_size, 0, 1);

	arm_rfft_q15(&S, samples, buffer_fft0);

	for (int i = 0; i< FFT_VISUAL_OFFSET; i++) { 						//per i bordi
		
		samples[i] = 0;
		
	}
	
	arm_cmplx_mag_q15(buffer_fft0, &samples[FFT_VISUAL_OFFSET], samples_size / 2);
}

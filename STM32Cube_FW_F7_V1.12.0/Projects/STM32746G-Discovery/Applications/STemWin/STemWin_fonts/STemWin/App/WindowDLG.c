
 //USER START (Optionally insert additional includes)
 //USER END
#include <stdbool.h>
#include <stdio.h>

#include "DIALOG.h"
#include "GUI.h"
#include "WM.h"
#include "dsp.h"



/*********************************************************************
*
*       Defines
*
**********************************************************************
*/
#define ID_WINDOW_0 (GUI_ID_USER + 0x00)
#define ID_CHECKBOX_2 (GUI_ID_USER + 0x01)
#define ID_CHECKBOX_0 (GUI_ID_USER + 0x02)
#define ID_CHECKBOX_1 (GUI_ID_USER + 0x03)
#define ID_TEXT_0 (GUI_ID_USER + 0x04)
#define ID_DROPDOWN_0 (GUI_ID_USER + 0x05)
#define ID_TEXT_1 (GUI_ID_USER + 0x06)
#define ID_DROPDOWN_1 (GUI_ID_USER + 0x07)
#define ID_GRAPH_0 (GUI_ID_USER + 0x08)
#define ID_SLIDER_0 (GUI_ID_USER + 0x09)
#define ID_TEXT_3 (GUI_ID_USER + 1)
#define ID_DROPDOWN_2 (GUI_ID_USER + 0x11)
#define ID_TEXT_2 (GUI_ID_USER + 0x12)
#define ID_CHECKBOX_3 (GUI_ID_USER + 0x13)
#define ID_DROPDOWN_3 (GUI_ID_USER + 0x14)
#define ID_TEXT_4 (GUI_ID_USER + 0x15)
#define ID_TEXT_5 (GUI_ID_USER + 0x16)
#define ID_TEXT_6 (GUI_ID_USER + 0x17)
#define ID_TEXT_7 (GUI_ID_USER + 0x18)

// USER START (Optionally insert additional defines)

// USER END

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/

// USER START (Optionally insert additional static data)
const char *dropdown_time[MAX_HSCALE] = {
	"500us/div",
	"1ms/div",
	"2ms/div",
	"5ms/div",
	"10ms/div",
	"20ms/div",
	"50ms/div",
	"100ms/div",
	"200ms/div",
	"500ms/div",
	"1s/div",
};

const char *dropdown_amplitude[MAX_VSCALE] = {
	"1mV/div",
	"2mV/div",
	"5mV/div",
	"10mV/div",
	"20mV/div",
	"50mV/div",
	"100mV/div",
	"200mV/div",
	"500mV/div",
	"1V/div",
};

const char *dropdown_channel[2] = {
	"CH 1",
	"CH 2",
};

const char *dropdown_trigger_mode[3] = {
	"PRE",
	"HALF",
	"POST",
};

//handle per gestire i widget
WM_HWIN hGraph;
GRAPH_DATA_Handle hData0;
GRAPH_DATA_Handle hData1;
GRAPH_SCALE_Handle hScale_v;
GRAPH_SCALE_Handle hScale_h;
WM_HWIN hItemMedia0, hItemRMS0, hItemMedia1, hItemRMS1;

//variabili di comunicazione con maintask
int livello_slider = 50;
int channel_1_enable = 1;
int channel_2_enable = 0;
bool fft_enable = true;
channel_t trigger_channel = CH_1;
trigger_conf trigger_mode = half;
edge_t fronte = up;


char media_format[] = "MEAN%d = %.2f";
char rms_format[] = "RMS%d = %.2f";

// USER END

/*********************************************************************
*
*       _aDialogCreate
*/


static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
  { WINDOW_CreateIndirect, "Window", ID_WINDOW_0, 0, 0, 600, 278, 0, 0x0, 0 },
	{ CHECKBOX_CreateIndirect, "CH 1", ID_CHECKBOX_0, 2, 28, 80, 20, 0, 0x0, 0 },
	{ CHECKBOX_CreateIndirect, "CH 2", ID_CHECKBOX_1, 2, 48, 80, 20, 0, 0x0, 0 },
	{ TEXT_CreateIndirect, "s/div", ID_TEXT_0, 2, 70, 80, 180, 0, 0x0, 0 },
  { DROPDOWN_CreateIndirect, "HORIZONTAL POSITION", ID_DROPDOWN_0, 0, 85, 80, 180, 0, 0x0, 0 },
	{ TEXT_CreateIndirect, "V/div", ID_TEXT_1, 2, 105, 80, 180, 0, 0x0, 0 },
	{ DROPDOWN_CreateIndirect, "VERTICAL POSITION", ID_DROPDOWN_1, 0, 120, 80, 160, 0, 0x0, 0 },
	{ TEXT_CreateIndirect, "TRIGGER MENU", ID_TEXT_2, 3, 150, 80, 180, 0, 0x0, 0 },
	{ SLIDER_CreateIndirect, "LEVEL", ID_SLIDER_0, 0, 210, 80, 30, 0, 0x0, 0 },
	{ DROPDOWN_CreateIndirect, "CHANNEL SELECTED", ID_DROPDOWN_2, 0, 165, 80, 60, 0, 0x0, 0 },
	{ GRAPH_CreateIndirect, "Graph", ID_GRAPH_0, 80, 0, GRAPH_SIZE_X, GRAPH_SIZE_Y-GRAPH_SIZE_BORDER_Y, 0, 0x0, 0 },
	{ CHECKBOX_CreateIndirect, "FRONTE", ID_CHECKBOX_3, 0, 242, 80, 20, 0, 0x0, 0 },
	{ CHECKBOX_CreateIndirect, "FFT", ID_CHECKBOX_2, 2, 1, 80, 20, 0, 0x0, 0 },
	{ DROPDOWN_CreateIndirect, "TRIGGER MODE", ID_DROPDOWN_3, 0, 190, 80, 60, 0, 0x0, 0 },
	{ TEXT_CreateIndirect, "MEAN1 = 1", ID_TEXT_4, GRAPH_SIZE_X-20, 6, 80, 20, 0, 0x0, 0 },
  { TEXT_CreateIndirect, "RMS1 = 1", ID_TEXT_5, GRAPH_SIZE_X-20, 22, 80, 20, 0, 0x0, 0 },
	{ TEXT_CreateIndirect, "RMS1 = 1", ID_TEXT_6, GRAPH_SIZE_X-20, 40, 80, 20, 0, 0x0, 0 },
	{ TEXT_CreateIndirect, "RMS1 = 1", ID_TEXT_7, GRAPH_SIZE_X-20, 58, 80, 20, 0, 0x0, 0 },
	// USER START (Optionally insert additional widgets)
  // USER END
};


void aggiorna_misure(void) {
	char buffer[15];
	
	sprintf(buffer, media_format, 1, result_mean0);
	TEXT_SetText(hItemMedia0, buffer);

	sprintf(buffer, rms_format, 1, result_rms0);				
	TEXT_SetText(hItemRMS0, buffer);		
	
	sprintf(buffer, media_format, 2, result_mean1);
	TEXT_SetText(hItemMedia1, buffer);
	
	sprintf(buffer, rms_format, 2, result_rms1);				
	TEXT_SetText(hItemRMS1, buffer);
	
	
}

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/

// USER START (Optionally insert additional static code)
// USER END

/*********************************************************************
*
*       _cbDialog
*/

static void _cbDialog(WM_MESSAGE * pMsg) {
  WM_HWIN hItem;
  int     NCode;
  int     Id;
  // USER START (Optionally insert additional variables)
  // USER END

  switch (pMsg->MsgId) {
		case WM_INIT_DIALOG:
			// Initialization of 'menu'
      hItem = WM_GetDialogItem(pMsg->hWin, ID_SLIDER_0);
      SLIDER_SetRange(hItem, 0, 100);
			SLIDER_SetValue (hItem, 50);
      livello_slider = 50;
    
			hItem = WM_GetDialogItem(pMsg->hWin, ID_CHECKBOX_0);
			CHECKBOX_SetText (hItem, "CH 1");
			CHECKBOX_SetState (hItem, 1);
      channel_1_enable = 1;
    
			hItem = WM_GetDialogItem(pMsg->hWin, ID_CHECKBOX_1);
			CHECKBOX_SetText (hItem, "CH 2");
			CHECKBOX_SetState (hItem, 0);
      channel_2_enable = 0;
    
			hItem = WM_GetDialogItem(pMsg->hWin, ID_CHECKBOX_2);
			CHECKBOX_SetText (hItem, "FFT");
			CHECKBOX_SetState (hItem, 0);
      
    
			hItem = WM_GetDialogItem(pMsg->hWin, ID_CHECKBOX_3);
			CHECKBOX_SetText (hItem, "FRONTE");			
			CHECKBOX_SetState (hItem, 0);
      fronte = up;
			
			hItemMedia0 = WM_GetDialogItem(pMsg->hWin, ID_TEXT_4);
			TEXT_SetTextColor(hItemMedia0, GUI_GREEN);
			
			hItemRMS0 = WM_GetDialogItem(pMsg->hWin, ID_TEXT_5);
			TEXT_SetTextColor(hItemRMS0, GUI_GREEN);
			
			hItemMedia1 = WM_GetDialogItem(pMsg->hWin, ID_TEXT_6);
			TEXT_SetTextColor(hItemMedia1, GUI_RED);
			
			hItemRMS1 = WM_GetDialogItem(pMsg->hWin, ID_TEXT_7);
			TEXT_SetTextColor(hItemRMS1, GUI_RED);
			aggiorna_misure();
			
			hGraph = WM_GetDialogItem(pMsg->hWin, ID_GRAPH_0);
			GRAPH_SetBorder(hGraph, GRAPH_SIZE_BORDER_X, GRAPH_SIZE_BORDER_X, GRAPH_SIZE_BORDER_Y, GRAPH_SIZE_BORDER_Y);     // SETTA I BORDI SULLO SCHERMO
			GRAPH_SetGridDistY(hGraph, (GRAPH_SIZE_Y-2*GRAPH_SIZE_BORDER_Y)/8);						// DISTANZA DIVISIONI VERTICALI
			GRAPH_SetGridDistX(hGraph,(GRAPH_SIZE_X-2*GRAPH_SIZE_BORDER_X)/10) ;					// DISTANZA DIVISIONI ORIZZONTALI
			GRAPH_SetLineStyleV(hGraph, 2);         		//METTE IL TRATTEGGIO DELLA GRIGLIA VERTICALE
			GRAPH_SetLineStyleH(hGraph, 2);          	//METTE IL TRATTEGGIO DELLA GRIGLIA ORIZZONTALE
			GRAPH_SetGridVis(hGraph, 1);              	// METTE LA GRIGLIA SULLO SCHERMO
			GUI_CURSOR_Show ();


      //-------------------------- SCALA ORIZZONTALE -------------------------------
			hScale_h=GRAPH_SCALE_Create(260, GUI_TA_RIGHT,GRAPH_SCALE_CF_HORIZONTAL,(GRAPH_SIZE_X - 2*GRAPH_SIZE_BORDER_X)/10);
      GRAPH_AttachScale(hGraph, hScale_h);
  		
      hItem = WM_GetDialogItem(pMsg->hWin, ID_DROPDOWN_0);
			for (int i = 0; i < MAX_HSCALE; i++) {
				DROPDOWN_AddString(hItem, dropdown_time[i]);     //LABLE SECONDI A DIVISIONE
			}
      DROPDOWN_SetSel (hItem, _500_us);
      horizontal_scale(_500_us);
      
      
      //-------------------------- SCALA VERTICALE -------------------------------
      hScale_v=GRAPH_SCALE_Create(26, GUI_TA_RIGHT,GRAPH_SCALE_CF_VERTICAL, (GRAPH_SIZE_Y-2*GRAPH_SIZE_BORDER_Y)/8);
      GRAPH_AttachScale(hGraph, hScale_v);

			hItem = WM_GetDialogItem(pMsg->hWin, ID_DROPDOWN_1);
			for (int i = 0; i < MAX_VSCALE; i++) {
				DROPDOWN_AddString(hItem, dropdown_amplitude[i]); //LABLE VOLT A DIVISIONE
			}
      DROPDOWN_SetSel (hItem, _500_mV);
      vertical_scale(_500_mV);
      
      //-------------------------- CANALE TRIGGER  -------------------------------		
			hItem = WM_GetDialogItem(pMsg->hWin, ID_DROPDOWN_2);
			DROPDOWN_AddString(hItem, dropdown_channel[0]); 
			DROPDOWN_AddString(hItem, dropdown_channel[1]);
      DROPDOWN_SetSel(hItem, CH_1);
      trigger_channel = CH_1;
			
			//-------------------------- TRIGGER MODE -------------------------------
			hItem = WM_GetDialogItem(pMsg->hWin, ID_DROPDOWN_3);
			DROPDOWN_AddString(hItem, dropdown_trigger_mode[0]); 
			DROPDOWN_AddString(hItem, dropdown_trigger_mode[1]);
			DROPDOWN_AddString(hItem, dropdown_trigger_mode[2]);
			DROPDOWN_SetSel (hItem, half);
			trigger_mode = half;
    break;
  case WM_NOTIFY_PARENT:
    Id    = WM_GetId(pMsg->hWinSrc);
    NCode = pMsg->Data.v;
    switch(Id) {
    case ID_DROPDOWN_0: // Notifications sent by 'menu'		
      switch(NCode) {
      case WM_NOTIFICATION_CLICKED:
        // USER START (Optionally insert code for reacting on notification message)
        // USER END
        break;
      case WM_NOTIFICATION_RELEASED:
        // USER START (Optionally insert code for reacting on notification message)
        // USER END
        break;
      case WM_NOTIFICATION_SEL_CHANGED:
				hItem = WM_GetDialogItem(pMsg->hWin, ID_DROPDOWN_0);
				h_scale_t scala = (h_scale_t)DROPDOWN_GetSel(hItem);  //cast
				horizontal_scale(scala);
        break;
      // USER START (Optionally insert additional code for further notification handling)
      // USER END
			}
			
	  case ID_DROPDOWN_1: // Notifications sent by 'menu'		
      switch(NCode) {
      case WM_NOTIFICATION_SEL_CHANGED:
        // USER START (Optionally insert code for reacting on notification message)
				hItem = WM_GetDialogItem(pMsg->hWin, ID_DROPDOWN_1);
				v_scale_t scala = (v_scale_t)DROPDOWN_GetSel(hItem);
				vertical_scale(scala);
        livello_trigger = (10 * fullscale_v * 4095 * livello_slider)/(3300 * 100);
				//in base al valore di ritorno della funzione,
				//oppure gestirlo direttamente nella funzione
        break;
			} 
			
	  case ID_DROPDOWN_2: // Notifications sent by 'menu'		
      switch(NCode) {
      case WM_NOTIFICATION_SEL_CHANGED:
				hItem = WM_GetDialogItem(pMsg->hWin, ID_DROPDOWN_2);
				trigger_channel = (channel_t)DROPDOWN_GetSel(hItem);
        break;
		}
			
    case ID_SLIDER_0:
      switch(NCode) {
      case WM_NOTIFICATION_VALUE_CHANGED:
				hItem = WM_GetDialogItem(pMsg->hWin, ID_SLIDER_0);
				livello_slider = SLIDER_GetValue(hItem);
        livello_trigger = (10 * fullscale_v * 4095 * livello_slider)/(330000);     
				//in base al valore di ritorno della funzione,
				//oppure gestirlo direttamente nella funzione
        break;
			} 
			
    case ID_CHECKBOX_0:				//CH_1 
      switch(NCode) {
      case WM_NOTIFICATION_VALUE_CHANGED:
				hItem = WM_GetDialogItem(pMsg->hWin, ID_CHECKBOX_0);
				channel_1_enable = CHECKBOX_GetState(hItem);			
        break;
			}     

		case ID_CHECKBOX_1:				//CH_2
      switch(NCode) {
      case WM_NOTIFICATION_VALUE_CHANGED:
				hItem = WM_GetDialogItem(pMsg->hWin, ID_CHECKBOX_1);
				channel_2_enable = CHECKBOX_GetState(hItem);
        break;
			}  
       
		case ID_CHECKBOX_2:				//FFT
      switch(NCode) {
      case WM_NOTIFICATION_VALUE_CHANGED:
				hItem = WM_GetDialogItem(pMsg->hWin, ID_CHECKBOX_2);
				fft_enable= CHECKBOX_GetState(hItem);
        break;
			} 
			
		case ID_DROPDOWN_3:				//trigger MODE
      switch(NCode) {
      case WM_NOTIFICATION_SEL_CHANGED:
				hItem = WM_GetDialogItem(pMsg->hWin, ID_DROPDOWN_3);
				trigger_mode = (trigger_conf)DROPDOWN_GetSel(hItem);
        break;
			} 

		case ID_CHECKBOX_3:					//FRONTE
      switch(NCode) {
      case WM_NOTIFICATION_VALUE_CHANGED:
				hItem = WM_GetDialogItem(pMsg->hWin, ID_CHECKBOX_3);
				fronte = (edge_t)CHECKBOX_GetState(hItem);
        break;
			}		
    break;	
		}
  default:
    WM_DefaultProc(pMsg);
    break;
  }
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
/*********************************************************************
*
*       CreateWindow
*/

WM_HWIN CreateWindow(void) {
	WM_HWIN hWin;
	hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate), _cbDialog, WM_HBKWIN, 0, 0);
  return hWin;
}


//// USER START (Optionally insert additional public code)
//// USER END

///*************************** End of file ****************************/

#include <stdint.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include "DIALOG.h"
#include "main.h"
#include "dsp.h"
#include "stm32f7xx_hal.h"
#include "arm_math.h"



extern uint16_t dma_input0[];
extern uint16_t dma_input1[];

extern WM_HWIN CreateWindow(void);

extern int channel_1_enable;
extern int channel_2_enable;
extern channel_t trigger_channel;
extern trigger_conf trigger_mode;
extern edge_t fronte;
extern bool fft_enable;
extern void aggiorna_misure(void);

//buffer per gestire l'elaborazione dimesnione (tripla per comodita' di gestione del pretrigger e post trigger)

uint16_t input0[3 * DMA_QUEQUE_SIZE] __attribute__((section("dtcm")));
uint16_t input1[3 * DMA_QUEQUE_SIZE] __attribute__((section("dtcm")));

int indice, indice_precedente = 0;
bool overrun = false;

void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc)
{
	if (indice != indice_precedente) {
		overrun = true;
	}
  indice += (DMA_QUEQUE_SIZE / 2);
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{	
	if (indice != indice_precedente) {
		overrun = true;
	}
  indice += (DMA_QUEQUE_SIZE / 2);
  if (indice >= (3 * DMA_QUEQUE_SIZE)) {
    indice = 0;
  }
}

int good_buffer_count = 0;

static bool transfer_data_automa(void) 
{
  __disable_irq();
  if (indice == indice_precedente) 
  {
    __enable_irq();
    return false;
  } 
  
  indice_precedente += (DMA_QUEQUE_SIZE / 2);
  if (indice_precedente >= (3 * DMA_QUEQUE_SIZE)) {
    indice_precedente = 0;
  }
	
	if (overrun) {
		//artificio che inibisce la visualizzazione
		//se per uno qualsiasi dei buffer si � verificato l'overrun
		overrun = false;
		good_buffer_count = 0;
	} else {
      good_buffer_count++;	
	}
  __enable_irq();
	
  switch(indice_precedente)
  {
    case DMA_QUEQUE_SIZE / 2:
      memcpy(input0, dma_input0, (DMA_QUEQUE_SIZE * sizeof(uint16_t)) / 2);
      memcpy(input1, dma_input1, (DMA_QUEQUE_SIZE * sizeof(uint16_t)) / 2);
    break;
    case DMA_QUEQUE_SIZE:
      memcpy(&input0[DMA_QUEQUE_SIZE / 2], &dma_input0[DMA_QUEQUE_SIZE / 2], (DMA_QUEQUE_SIZE * sizeof(uint16_t)) / 2);
      memcpy(&input1[DMA_QUEQUE_SIZE / 2], &dma_input1[DMA_QUEQUE_SIZE / 2], (DMA_QUEQUE_SIZE * sizeof(uint16_t)) / 2);
    break;
    case (3 * DMA_QUEQUE_SIZE) / 2:
      memcpy(&input0[DMA_QUEQUE_SIZE], &dma_input0[0], (DMA_QUEQUE_SIZE * sizeof(uint16_t)) / 2);
      memcpy(&input1[DMA_QUEQUE_SIZE], &dma_input1[0], (DMA_QUEQUE_SIZE * sizeof(uint16_t)) / 2);
    break;
    case (2 * DMA_QUEQUE_SIZE):
      memcpy(&input0[3 *DMA_QUEQUE_SIZE / 2], &dma_input0[DMA_QUEQUE_SIZE / 2], (DMA_QUEQUE_SIZE * sizeof(uint16_t)) / 2);
      memcpy(&input1[3 *DMA_QUEQUE_SIZE / 2], &dma_input1[DMA_QUEQUE_SIZE / 2], (DMA_QUEQUE_SIZE * sizeof(uint16_t)) / 2);
    break;
    case (5 * DMA_QUEQUE_SIZE) / 2:
      memcpy(&input0[2 * DMA_QUEQUE_SIZE], &dma_input0[0], (DMA_QUEQUE_SIZE * sizeof(uint16_t)) / 2);
      memcpy(&input1[2 * DMA_QUEQUE_SIZE], &dma_input1[0], (DMA_QUEQUE_SIZE * sizeof(uint16_t)) / 2);
    break;
    case 0: //3 * DMA_QUEQUE_SIZE:
      memcpy(&input0[5 *DMA_QUEQUE_SIZE / 2], &dma_input0[DMA_QUEQUE_SIZE / 2], (DMA_QUEQUE_SIZE * sizeof(uint16_t)) / 2);
      memcpy(&input1[5 *DMA_QUEQUE_SIZE / 2], &dma_input1[DMA_QUEQUE_SIZE / 2], (DMA_QUEQUE_SIZE * sizeof(uint16_t)) / 2);
		
			if (good_buffer_count == 6) {
				good_buffer_count = 0;
				return true;
			} else {
				good_buffer_count = 0;
				return false;
			}
		break;
  }
  return false;
}

void fft(uint16_t *samples, int len) 
{
	q15_t *pInput0_fractional = (q15_t *)samples;	//l'operazione di conversione viene effettuata in place per ottimizzare la memoria

/*
	for (int i = 0; i < 1024; i++)		//digit
	{
		samples[i] = 4095;
	}
*/
	converti_in_fractional_normalizzati(samples, pInput0_fractional, len);
	
	calcolo_fft(pInput0_fractional, len);
	
	converti_da_fractional_normalizzati(pInput0_fractional, samples, len);
	
	for (int i = FFT_VISUAL_OFFSET + 1; i < GRAPH_SIZE_X; i++) 
	{
		samples[i] = ((int32_t)(141L * samples[i]))/100;
	}
}

void calcola_misure(void)
{
	//quanti periodi interi ci sono nella mia finestra di osservazione
	//prima nel primo pezzo del buffer e poi nel secondo
	int start = trigger(&input0[0], GRAPH_SIZE_X, livello_trigger, fronte, 60);
	int stop = GRAPH_SIZE_X + trigger(&input0[GRAPH_SIZE_X], GRAPH_SIZE_X, livello_trigger, fronte, 60);
	result_mean0=calculate_mean(&input0[start], stop - start);
	result_rms0=calculate_rms(&input0[start], stop - start);
	
	start = trigger(&input1[0], GRAPH_SIZE_X, livello_trigger, fronte, 60);
	stop = GRAPH_SIZE_X + trigger(&input1[GRAPH_SIZE_X], GRAPH_SIZE_X, livello_trigger, fronte, 60);
	result_mean1=calculate_mean(&input1[start], stop - start);
	result_rms1=calculate_rms(&input1[start], stop - start);
}

void MainTask(void) {
	WM_HWIN hWin;
  uint32_t tick = HAL_GetTick();
  
  //memset(input0, 0, 2*3*DMA_QUEQUE_SIZE);
  //memset(input1, 0, 2*3*DMA_QUEQUE_SIZE);
  
	GUI_SetBkColor(GUI_BLACK);
	GUI_Clear();

  hWin = CreateWindow();
	
	hData0 = GRAPH_DATA_YT_Create(GUI_GREEN, GRAPH_SIZE_X, (const short *)input0, GRAPH_SIZE_X);
	GRAPH_AttachData (hGraph, hData0);
	hData1 = GRAPH_DATA_YT_Create(GUI_RED, GRAPH_SIZE_X, (const short *)input1, GRAPH_SIZE_X);
	GRAPH_AttachData (hGraph, hData1);
  
	WM_PaintWindowAndDescs(hWin);
	GUI_Exec();
	
	create_hanning_window(1024);
	
	while (1)
	{ 
    if ((HAL_GetTick() - tick) > 100) //ongi cento ms 
    {
      tick = HAL_GetTick();

      //TODO: verificare a che serve questa chuiamata
      //
			//HAL_GPIO_WritePin(ARDUINO_D4_GPIO_Port, ARDUINO_D4_Pin, GPIO_PIN_SET);
			aggiorna_misure();
			GUI_Exec();
			//HAL_GPIO_WritePin(ARDUINO_D4_GPIO_Port, ARDUINO_D4_Pin, GPIO_PIN_RESET);
			

      //TODO: per frequenze di campionamento elevate non riusciamo a trasferire tutti i campioni perche' la guiexec dura troppo tempo
    }
		
    // codice di elaborazione
    if (transfer_data_automa()) 
    {
			volatile int pos = 0;
			if (trigger_channel == 0){
				//il segnale � in digit abbiamo considerato un rumore dell' 1,5% circa
				//60 � circa 1,5% di 4095
				pos	= trigger(&input0[GRAPH_SIZE_X], GRAPH_SIZE_X, livello_trigger, fronte, 60); 	
			} else {
				pos	= trigger(&input1[GRAPH_SIZE_X], GRAPH_SIZE_X, livello_trigger, fronte, 60);
			}
			//gestione pre post trigger
			pos += GRAPH_SIZE_X;

			if (trigger_mode == pre) {
				 pos -= (GRAPH_SIZE_X);
			}
			if (trigger_mode == half){
				 pos -= (GRAPH_SIZE_X / 2);
			}
			/*				
			if (trigger_mode == post){
				 pos;
			}
			*/
			
			calcola_misure();
			
			GRAPH_DATA_YT_Clear(hData0);
			GRAPH_DATA_YT_Clear(hData1);
			
			if (channel_1_enable == 1)
			{
				if (fft_enable)
				{
					fft(input0, 1024);
					pos = 0;
				} 
				
				apply_vertical_scale(&input0[pos], DMA_QUEQUE_SIZE);
				
				for (int i = 0; i < GRAPH_SIZE_X; i++)
				{
					GRAPH_DATA_YT_AddValue(hData0, input0[i + pos]);
				}
			}

			if (channel_2_enable == 1)
			{
				if (fft_enable)
				{
					fft(input1, 1024);
					pos = 0;
				} 
				
				apply_vertical_scale(&input1[pos], GRAPH_SIZE_X);
				
				for (int i = 0; i < GRAPH_SIZE_X; i++)
				{
					GRAPH_DATA_YT_AddValue (hData1, input1[i + pos]);
				}		
			}
		}
	}
}
